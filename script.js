import jokes from "./jokes-array.js";

/*

const feedRandomJoke = document.getElementById("generate-jokes");

const feedRandomNerdyJoke = document.getElementById("generate-nerdy-jokes");

const feedAllJokes = document.getElementById("generate-all-jokes");

document.getElementById("generate-jokes").addEventListener("click", () => {
    document.getElementById("joke_1").innerHTML = "Hello World";
});

document.getElementById("myBtn").addEventListener("click", () => {
    document.getElementById("demo").innerHTML = "Hello World";
});

feedRandomNerdyJoke.addEventListener("click", () => {

    });

feedAllJokes.addEventListener("click", () => {

    }); 

*/

let randomJoke_1 = generateRandomJoke(jokes);
let randomJoke_2 = generateRandomJoke(jokes);
let randomJoke_3 = generateRandomJoke(jokes);

let randomJokeNerdy_1 = generateRandomNerdyJoke(jokes);
let randomJokeNerdy_2 = generateRandomNerdyJoke(jokes);
let randomJokeNerdy_3 = generateRandomNerdyJoke(jokes);

/*

const randomJoke_1_Insert = document.createElement("p");
const randomJoke_2_Insert = document.createElement("p");
const randomJoke_3_Insert = document.createElement("p");

const randomJokeNerdy_1_Insert = document.createElement("p");
const randomJokeNerdy_2_Insert = document.createElement("p");
const randomJokeNerdy_3_Insert = document.createElement("p");

randomJoke_1_Insert.innerHTML = randomJoke_1;
randomJoke_2_Insert.innerHTML = randomJoke_2;
randomJoke_3_Insert.innerHTML = randomJoke_3;

randomJokeNerdy_1_Insert.innerHTML = randomJokeNerdy_1;
randomJokeNerdy_2_Insert.innerHTML = randomJokeNerdy_2;
randomJokeNerdy_3_Insert.innerHTML = randomJokeNerdy_3;

*/

function generateRandomJoke (array) {
    let randomJoke = jokes[Math.floor(Math.random() * array.length)];
    return randomJoke;
};

function generateRandomNerdyJoke (array) {
    let randomCounter = Math.floor(Math.random() * array.length);   
    let filteredJokes = [];
   
    for (let counter = 0; counter < array.length; counter++) {
        if (array[counter].categories[0] === "nerdy") {
            filteredJokes.push(array[counter]);
        } else {
            continue;
        }
    }

    let randomNerdyJoke = filteredJokes[Math.floor(Math.random() * filteredJokes.length)];      
    return randomNerdyJoke;

/*  Non-working solution:  

    filteredJokes = array.filter((element) => { 
        element.categories[0] === "nerdy";
    });

    let randomNerdyJoke = filteredJokes[randomCounter];


    Non-working solution:  

    array.forEach((element, index, array) => {
        index = randomCounter;
        if (array[index].categories.includes("nerdy")) {
            randomNerdyJoke = element;
        } 
    });
*/  
};

function generateAllJokes (array) {
    let allJokesArray = [];
    for (let counter_2 = 0; counter_2 < array.length; counter_2++) {
        allJokesArray.push(array[counter_2].joke);
    }
    return allJokesArray;
}

console.log(randomJoke_1);
console.log(randomJoke_2);
console.log(randomJoke_3);

console.log(randomJokeNerdy_1);
console.log(randomJokeNerdy_2);
console.log(randomJokeNerdy_3);

console.log(generateAllJokes(jokes));